<!DOCTYPE html>
<html>
  <head>
    <title>VSHealth</title>

    <link rel="shortcut icon" href="img/favicon.ico">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <link rel="stylesheet" href="index.css">
  </head>
  <body class="error">
    <div class="title">
      <nav class="navbar navbar-default">
       <div class="container-fluid">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button-->
           <!--a class="navbar-brand" href="#"><img src="img/logo.svg" height=45 /></a-->
         </div>
         <div class="collapse navbar-collapse" id="myNavbar">
           <ul class="nav navbar-nav">
             <li class="active"><a href="index.php"><img src="img/logo.svg" alt="VSHealth" height=25 class="nopad nomarg" /></a></li>
             <li><a href="track.php">Track</a></li>
           </ul>
           <ul class="nav navbar-nav navbar-right">
             <li><a href="report.php"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
             <li><a href="login/login.php"><span class="glyphicon glyphicon-user"></span> Login</a></li>
             <li><a href="https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http%3A%2F%2F127.0.0.1%2Fvshealth%2Flogin%2Flogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
           </ul>
         </div>
       </div>
     </nav>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 col-xs-12 col-lg-offset-2 col-md-offset-1 content">
          <h1>
            Oh, No!
          </h1>
          <hr />

          <p>
            Our site has encountered an error. Try returning <a href="index.php">Home</a>
          </p>

        </div>
      </div>
  </div>

  </body>
</html>
