<?php

require_once '/login/config.php';

?>
<html>
  <head>
    <title>VSHealth</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="index.css">
  </head>
  <body>
    <div class="title">
      <nav class="navbar navbar-default">
       <div class="container-fluid">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <!--a class="navbar-brand" href="#"><img src="img/logo.svg" height=45 /></a-->
         </div>
         <div class="collapse navbar-collapse" id="myNavbar">
           <ul class="nav navbar-nav">
             <li><a href="index.php"><img src="img/logo.svg" alt="VSHealth" height=25 class="nopad nomarg" /></a></li>
             <li><a href="track.php">Track</a></li>
           </ul>
           <ul class="nav navbar-nav navbar-right">
             <ul class="nav navbar-nav navbar-right">
               <?php
               if (isset($_SESSION["user_id"])) {
                 echo '<li><a href="report.php"><span class="glyphicon glyphicon-user"></span> '.$_SESSION["name"].'</a></li>';
                 echo '<li><a href="'.LOGOUT_URL.'"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>';
               }else {
                 echo '<li><a href="login/login.php"><span class="glyphicon glyphicon-user"></span> Login</a></li>';
                 #echo `<li><a href="https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http%3A%2F%2F127.0.0.1%2Fvshealth%2Flogin%2Flogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>`;
               }
               ?>
           </ul>
         </div>
       </div>
     </nav>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-1 col-xs-0">
        </div>
        <div class="col-lg-8 col-md-10 col-xs-12 content">
          <h1>
            Please enter some first time information:
          </h1>
            <form class="form-horizontal" role="form" action="newuser.php" method="post">
              <div class="form-group">
                <label class="control-label col-sm-3" for="heightft">Height - ft:</label>
                <div class="col-sm-4">
                  <select class="form-control" id="heightft" name="heightft">
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                  </select>
                </div>

                <label class="control-label col-sm-1" for="heightin">in:</label>
                <div class="col-sm-4">
                  <select class="form-control" id="heightin" name="heightin">
                    <option>0</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    <option>11</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-sm-3" for="weight">Weight:</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="weight" name="weight" placeholder="In lbs" required="true">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-sm-3" for="age">Age:</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="age" name="age" placeholder="How old are you?" required="true">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-sm-3" for="sex">Gender:</label>
                <div class="col-sm-9">
                  <label class="radio-inline"><input type="radio" name="sex" value="0">Male</label>
                  <label class="radio-inline"><input type="radio" name="sex" value="1">Female</label>
                </div>
              </div>



              <div class="form-group">
                <label class="control-label col-sm-3" for="active">Activity level:</label>
                <div class="col-sm-9">
                  <select class="form-control" id="active" name="active">
                    <option value="0">None</option>
                    <option value="1">Light</option>
                    <option value="2">Moderate</option>
                    <option value="3">Heavy</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-sm-3" for="goal" name="goal">Goal:</label>
                <div class="col-sm-9">
                  <select class="form-control" id="goal" name="goal">
                    <option value="0">Lose</option>
                    <option value="1">Maintain</option>
                    <option value="2">Gain</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-offset-10 col-sm-2">
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
              </div>
            </form>

        </div>
      </div>
  </div>

  </body>
</html>
