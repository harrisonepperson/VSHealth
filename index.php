<!DOCTYPE html>
<?php
require_once '/login/config.php';
?>
<html>
  <head>
    <title>VSHealth</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="img/favicon.ico">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="index.css">
  </head>
  <body>
      <nav class="navbar navbar-default">
       <div class="container-fluid">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="/index.php"><img src="img/logo.svg" alt="VSHealth" height=25 class="nopad nomarg" /></a>
         </div>
         <div class="collapse navbar-collapse" id="myNavbar">
           <ul class="nav navbar-nav">
             <!--li class="active"><a href="index.php"><img src="img/logo.svg" alt="VSHealth" height=25 class="nopad nomarg" /></a></li>-->
             <li><a href="track.php">Track</a></li>
           </ul>
           <ul class="nav navbar-nav navbar-right">
             <?php
             if (isset($_SESSION["user_id"])) {
               echo '<li><a href="report.php"><span class="glyphicon glyphicon-user"></span>'.$_SESSION["name"].'</a></li>';
               echo '<li><a href="'.LOGOUT_URL.'"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>';
             }else {
               echo '<li><a href="login/login.php"><span class="glyphicon glyphicon-user"></span> Login</a></li>';
               #echo `<li><a href="https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http%3A%2F%2F127.0.0.1%2Fvshealth%2Flogin%2Flogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>`;
             }
             ?>
           </ul>
         </div>
       </div>
     </nav>

    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 col-xs-12 col-lg-offset-2 col-md-offset-1 content">
          <h1>
            Welcome to VSHealth
          </h1>
          <hr />
          <p>
            VSHealth offers an easy and convenient way to track the health of students and faculty at Valdosta State University.  Complete with Nutrition information from food sources around campus, VSHealth allows you to stay healthy with ease.
          </p>

          <p>
            Using VSHealth is simple. Log in using your google account, fill out the Bio-form, and update your info throughout the week to track your health and meet your goal.
          </p>

          <div class="container">
            <div class="row">
              <div class="col-lg-2 col-md-3 col-xs-12 col-lg-offset-1 col-md-offset-0">
                <h4>
                  Krunal
                </h4>
                <hr />
                <p>
                  I use VSHealth to stay fit and simplify healthy living in college.
                </p>
              </div>
              <div class="col-lg-2 col-md-3 col-xs-12">
                <h4>
                  Colby
                </h4>
                <hr />
                <p>
                  I use VSHealth to remind me not to eat Chick-fil-a daily.
                </p>
              </div>
              <div class="col-lg-2 col-md-3 col-xs-12">
                <h4>
                  Landon
                </h4>
                <hr />
                <p>
                  I use VSHealth to try and maintain a healthy lifestyle while being distracted by a busy school schedule.
                </p>
              </div>
            </div>
          </div>

        </div>
      </div>
  </div>

  </body>
</html>
