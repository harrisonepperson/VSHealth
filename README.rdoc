== VSHealth editing information - IMPORTANT

The SQL server is hosted on gearhost.
-
The server is: mysql2.gear.host  
The username is: generic  
The password is: vshealthiscool!  

The database can be modified from mySQL workbench.

To make changes to the live site on http://vshealth.gear.host/
add another git remote, https://vshealth.scm.gear.host:443/vshealth.git
Push all finished changes both to GitLab and to the above repository. It's very
similar to publishing to Heroku.

Command to run once:
git remote add deploy https://vshealth.scm.gear.host:443/vshealth.git

Then to push live site changes just commit all changes and run:
git push deploy (branchname)  
Username is: $vshealth  
Password is: fkaPFnJZyEJWQx4k2F6nW80ryrZpemQyk1zi1PJmZcJTjCNDPnxNPK13pKPz  
