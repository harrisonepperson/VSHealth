<?php
require_once '/login/config.php';
?>
<html>
  <head>
    <title>VSHealth</title>
    <link rel="shortcut icon" href="img/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="index.css">
  </head>
  <body>
    <div class="title">
      <nav class="navbar navbar-default">
       <div class="container-fluid">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="/index.php"><img src="img/logo.svg" alt="VSHealth" height=25 class="nopad nomarg" /></a>
         </div>
         <div class="collapse navbar-collapse" id="myNavbar">
           <ul class="nav navbar-nav">
             <!--li><a href="index.php"><img src="img/logo.svg" alt="VSHealth" height=25 class="nopad nomarg" /></a></li-->
             <li class="active"><a href="track.php">Track</a></li>
           </ul>
           <ul class="nav navbar-nav navbar-right">
             <ul class="nav navbar-nav navbar-right">
               <?php
               if (isset($_SESSION["user_id"])) {
                 echo '<li><a href="report.php"><span class="glyphicon glyphicon-user"></span> '.$_SESSION["name"].'</a></li>';
                 echo '<li><a href="'.LOGOUT_URL.'"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>';
               }else {
                 echo '<li><a href="login/login.php"><span class="glyphicon glyphicon-user"></span> Login</a></li>';
                 #echo `<li><a href="https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http%3A%2F%2F127.0.0.1%2Fvshealth%2Flogin%2Flogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>`;
               }
               ?>
           </ul>
         </div>
       </div>
     </nav>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-1 col-xs-0">
        </div>
        <div class="col-lg-8 col-md-10 col-xs-12 content">
          <h1>Track your Health</h1>
          <hr />
          <form class="form-horizontal" role="form" action="trackuser.php" method="post">
            <div class="form-group">
              <label class="control-label col-sm-3" for="calories">Calorie Intake:</label>
              <div class="col-sm-9">
                <input type="number" class="form-control" id="calories" name="calories" placeholder="In calories" required="true">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-3" for="water">Water Intake:</label>
              <div class="col-sm-9">
                <input type="number" class="form-control" id="water" name="water" placeholder="In oz" required="true">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-3" for="minutes">Active Minutes:</label>
              <div class="col-sm-9">
                <input type="number" class="form-control" id="minutes" name="minutes" placeholder="In minutes" required="true">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-3" name="sleep" for="sleep">Sleep:</label>
              <div class="col-sm-9">
                <input type="number" class="form-control" id="sleep" name="sleep" placeholder="In minutes" required="true">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-10 col-sm-2">
                <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>

  </body>
</html>
