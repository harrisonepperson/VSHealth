<?php
require_once '/login/config.php';
require_once 'dbconnect.php';

$currentUser = $_SESSION["id"];
$result = $conn->query('SELECT * FROM userlist WHERE id = '.$currentUser);
$result2 = $conn->query('SELECT * FROM fitdata WHERE id = '.$currentUser);
$userData = $result->fetch_assoc();
$dataDay = $result2->fetch_assoc();

$calOverUnder = ($userData['recordedCal'] - $dataDay['calsConsumed']);
if($calOverUnder < 0) {

} else {

}
$waterLevel = (($userData['weight']/2) - $dataDay['waterIntake']);
if($userData['age'] < 18) {
  $recSleep = 600;
} else if($userData['age'] < 64) {
  $recSleep = 480;

} else {
  $recSleep = 420;
}
$sleepOut = ($recSleep - $dataDay['hoursSlept']);
if($userData['goal'] == 0)
{
  $g = 500;
} else if($userData['goal'] == 1) {
  $g = 0;
} else {
  $g = -500;
}
$totalNum = ($userData['recordedCal'] - $g + (5*($userData['weight']/2)) + $recSleep);
$dailyGrade = ($dataDay['calsConsumed'] - $dataDay['calsBurned'] + (5*$dataDay['waterIntake']) + ($dataDay['hoursSlept']));
$grade = (($totalNum/$dailyGrade) * 100);
if($grade >= 100)
{
  $trueGrade = ($grade - 100);
} else {
  $trueGrade = (100 - $grade);
}
if($trueGrade <= 15){
  $letterGrade = "A";
} else if($trueGrade <= 30){
  $letterGrade = "B";
} else if($trueGrade <= 45){
  $letterGrade = "C";
} else if($trueGrade <= 60){
  $letterGrade = "D";
} else {
  $letterGrade = "F";
}
?>
<html>
  <head>
    <title>VSHealth</title>
    <link rel="shortcut icon" href="img/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="index.css">
  </head>
  <body>
    <div class="title">
      <nav class="navbar navbar-default">
       <div class="container-fluid">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="/index.php"><img src="img/logo.svg" alt="VSHealth" height=25 class="nopad nomarg" /></a>
         </div>
         <div class="collapse navbar-collapse" id="myNavbar">
           <ul class="nav navbar-nav">
             <!--li><a href="index.php"><img src="img/logo.svg" alt="VSHealth" height=25 class="nopad nomarg" /></a></li-->
             <li ><a href="track.php">Track</a></li>
             <li class="active"><a href="report.php">Report</a></li>

           </ul>
           <ul class="nav navbar-nav navbar-right">
             <ul class="nav navbar-nav navbar-right">
               <?php
               if (isset($_SESSION["user_id"])) {
                 echo '<li><a href="report.php"><span class="glyphicon glyphicon-user"></span> '.$_SESSION["name"].'</a></li>';
                 echo '<li><a href="'.LOGOUT_URL.'"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>';
               }else {
                 echo '<li><a href="login/login.php"><span class="glyphicon glyphicon-user"></span> Login</a></li>';
                 #echo `<li><a href="https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http%3A%2F%2F127.0.0.1%2Fvshealth%2Flogin%2Flogout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>`;
               }
               ?>
           </ul>
         </div>
       </div>
     </nav>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 col-xs-12 col-lg-offset-2 col-md-offset-1 content">
          <h1>
            Daily Health Report
          </h1>
          <hr />

          <table class="customTable">
            <colgroup>
              <col width=20%>
              <col width=80%>
            </colgroup>
            <tr>
              <td>Calorie (Over/Under)</td>
              <td><?php echo $calOverUnder; ?></td>
            </tr>
            <tr>
              <td>Calories Burned:</td>
              <td><?php echo $dataDay['calsBurned']; ?></td>
            </tr>
            <tr>
              <td>Water Level:</td>
              <td><?php echo $waterLevel; ?></td>
            </tr>
            <tr>
              <td>Sleep:</td>
              <td><?php echo $sleepOut; ?></td>
            </tr>
            <tr>
              <td>Daily Total:</td>
              <td><?php echo $totalNum; ?></td>
            </tr>
            <tr>
              <td>Daily Grade:</td>
              <td><?php echo $letterGrade; ?></td>
            </tr>
          </table>

        </div>
      </div>
  </div>

  </body>
</html>
